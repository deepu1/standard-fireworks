$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
    var sourceSwap = function () {
      var $this = $(this);
      var newSource = $this.data('src');
      $this.data('src', $this.attr('src'));
      $this.attr('src', newSource);
    }
    $(function () {
      $('img.icon-hover').hover(sourceSwap, sourceSwap);
    });
    $('.header-search-wrap').click(function(){
      $('.header-search-wrap input').toggle()
    })
    $('.header-search-wrap input').click(function(event){
      event.stopImmediatePropagation();
    });
    $('.login-popup-wrap').click(function(){
      $('.login-popup').toggle()
    });
    $('.product-sorting .dropdown-menu li').click(function(){
      var category = $(this).html()
      $('.sort-category').html(category)
    });
    $('.mob-close').click(function(){
      $('#navbar').removeClass("in")
    });
    $('.mobile-filter-wrap').click(function(){
      $('.filter-wrap').toggle()
    });
    var body_height = $('.main-page').height()-$('.navbar').height()+10;
    $('.filter-wrap').height(body_height);
    $('.navbar-height').css("height",body_height);
    $("#selectall").change(function () {
      $(".filter-wrap input:checkbox").prop('checked', $(this).prop("checked"));
    });
    $("#selectallmob").change(function () {
      $(".filter-wrap input:checkbox").prop('checked', $(this).prop("checked"));
    });
    $('.clear-all-btn').click(function(e){
      e.preventDefault();
      $(".filter-wrap input:checkbox").removeAttr('checked');
    });
    $('.page-close-button').click(function(){
      parent.history.back();
      return false;
    });
    $('.filter-wrap input:checkbox').change(function() {
      if ($('.filter-wrap input:checkbox').is(":checked")) {
                $(".filter-bttn-wrap").show();
            } else {
                $(".filter-bttn-wrap").hide();
            }
    });
});